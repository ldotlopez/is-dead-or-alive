module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    uglify: {
      options: {
        sourceMap: false,
        preserveComments: false,
        compress: {
          drop_console: true
        },
      },
      target: {
        files: {
          'build/js/<%= pkg.name %>.v<%= pkg.version %>.min.js': [
            'src/components/jquery/dist/jquery.js',
            'src/components/bootstrap/dist/js/bootstrap.js',
            'src/components/angular/angular.js',
            'src/components/angular-route/angular-route.js',
            'src/components/angular-translate/angular-translate.js',
            'src/js/app.js',
            'src/js/controllers.js'
          ],
        }
      }
    },

    cssmin: {
      options: {
        keepSpecialComments: 0
      },
      target: {
        files: {
          'build/css/<%= pkg.name %>.v<%= pkg.version %>.min.css': [
            'src/components/bootstrap/dist/css/bootstrap.css',
            'src/css/app.css'
          ]
        }
      }
    },

    copy: {
      fonts: {
        expand: true,
        flatten: true,
        src: 'src/components/bootstrap/fonts/*',
        dest: 'build/fonts/'
      },
      html: {
        expand: true,
        flatten: true,
        src: 'src/partials/*',
        dest: 'build/partials/'
      }
    },

    processhtml: {
      target: {
        files: {
          'build/index.html': ['src/index.html']
        }
      }
    }

  });
  
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-processhtml');

  // Default task(s).
  grunt.registerTask('default',['uglify', 'cssmin', 'copy', 'processhtml']);

};
