'use strict';

/* App module */

var idoaApp = angular.module('idoaApp', [
  'ngRoute',
  'idoaControllers',
  'pascalprecht.translate'
]);

idoaApp.config(['$routeProvider', '$locationProvider', '$translateProvider', function($routeProvider, $locationProvider, $translateProvider) {
	$routeProvider
		.when('/s/:query?', {
			controller: 'searchCtrl',
			templateUrl: 'partials/search.html',
			isSuggestion: false,
		})

		.when('/about', {
			templateUrl: function ($routeParameters) {
				var language = $translateProvider.use();
				var suffix = '.html';

				if (!(language in $translateProvider.translations())) {
					language = 'en';
				}
				return 'partials/about-'+language+'.html';
			}
		})
		.otherwise({
			redirectTo: '/s/'
		});
		
	$locationProvider.html5Mode(false).hashPrefix('!');

	$translateProvider
		.translations('en', {
			'SEARCH_PLACEHOLDER': 'Search for someone…',
			'ABOUT_LABEL': "About 'is dead or alive?'",
			'SEARCHING_LABEL': 'Searching…',
			'SUGGESTIONS_LABEL': 'Try with one of these:',
			'ALIVE_LABEL': 'alive',
			'DEAD_LABEL': 'dead',
			'IS_VERB': 'is',

			/* Some errors */
			'UNKNOW_LANGUAGE_ERROR': "Unknow language '{{language}}'",

			/* Errors from exceptions */
			'NOT_A_PERSON_ERROR': "There isn't information for '{{who}}' (or isn't a person…)",
			'NO_PAGE_ERROR': "There isn't information for '{{who}}' (or isn't a person…)",
			'NO_INFORMATION_ERROR': "There isn't enough information for {{who}}",
			'NETWORK_FAILURE_ERROR': "The internets isn't working properly now, try again later",
			'WP_REDIRECT_ERROR': "'The internets' isn't working properly now, try again later"
		})

		.translations('es', {
			'SEARCH_PLACEHOLDER': 'Buscar a alguien…',
			'ABOUT_LABEL': "Acerca de 'is dead or alive?'",
			'SEARCHING_LABEL': 'Buscando…',
			'SUGGESTIONS_LABEL': 'Prueba con alguno de estos:',
			'ALIVE_LABEL': 'vivo/a',
			'DEAD_LABEL': 'muerto/a',
			'IS_VERB': 'está',
			'UNKNOW_LANGUAGE_ERROR': 'Idioma desconocido: {{language}}',
			
			'NOT_A_PERSON_ERROR': "No hay información sobre '{{who}}' (o no es una persona…)",
			'NO_PAGE_ERROR':  "No hay información sobre '{{who}}' (o no es una persona…)",
			'NO_INFORMATION_ERROR': "No hay información suficiente sobre {{who}}",
			'NETWORK_FAILURE_ERROR': "'La internet' no está funcionando adecuadamente, prueba más tarde",
			'WP_REDIRECT_ERROR':  "'La internet' no está funcionando adecuadamente, prueba más tarde"
		})
		.registerAvailableLanguageKeys(['en', 'es'], {
			'en_EN': 'en',
			'en_UK': 'en',
			'es_ES': 'es'
		})
		.fallbackLanguage('en')
		.determinePreferredLanguage();
}]);
