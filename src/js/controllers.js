'use strict';

var WP_DATA = {
	'en': {
		'urlPrefix': 'en',
		'birthRegexp': /\|\s*(?:birth_date|DATE.OF.BIRTH)\s*=\s*(\S+)/i,
		'deathRegexp': /\|\s*(?:death_date|DATE.OF.DEATH)\s*=\s*(\S+)/i
	},
	'es': {
		'urlPrefix': 'es',
		'birthRegexp': /\|\s*(?:fechanac|fechadenacimiento|fecha.nacimiento|fecha.de.nacimiento|nacimiento)\s*=\s*(\S+)/i,
		'deathRegexp': /\|\s*(?:muerte|fechamuerte|fecha.de.fallecimiento|fechafallecimiento|fallecimiento|fecha.de.defunción)\s*?=\s*?(\S+)/i
	},
}

/* gettext:
 * Utility function for translate text thru angular's $translate
 * angular deps: []
 */
function gettext($translate, errorMsgId) {
	var interpolateArguments = arguments[2] || {};
	
	return $translate.instant(errorMsgId, interpolateArguments);
}


var idoaControllers = angular.module('idoaControllers', []);

/*
 * languageCtrl:
 * Controls language query and selection.
 */
idoaControllers.controller('languageCtrl', ['$scope','$translate',
	function($scope, $translate) {
		
		/* $scope.updateLanguages:
		 * update $scope.languages binding with convenient languages from $translate object
		 * angujar deps: [$scope, $translate]
		 */
		$scope.updateLanguages = function() {
			var currLanguage = $translate.use();
		
			var languages = [];
			for (var language in WP_DATA) {
				languages.push({
					'name': language,
					'active': (language == currLanguage)
				});
			}
			console.log("[language] update languages: '"+languages+"'");
			$scope.languages = languages;
		}


		/* $scope.setLanguage:
		 * update $scope.languages binding with convenient languages from $translate object
		 * angujar deps: [$scope, $translate]
		 */
		$scope.setLanguage = function (key) {
			if (!(key in WP_DATA)) {
				console.log("[language] unknow language '"+key+"'");
				$scope.error = gettext($translate, 'UNKNOW_LANGUAGE_ERROR', {'language': key});
				return;
			}

			console.log("[language] switch to '"+key+"'");
			$translate.use(key);
			$scope.updateLanguages();
		}


		$scope.updateLanguages();

	}
]);


/*
 * searchCtrl:
 * Query wikipedia or redirect to suggestions
 */
idoaControllers.controller('searchCtrl', ['$scope', '$http', '$location', '$q', '$route', '$routeParams', '$translate',
	function($scope, $http, $location, $q, $route, $routeParams, $translate) {

		/* fixNameCase:
		 * Converts 'name' in a Camel Case form. p.ex. 'some name' -> 'Some Name'
		 * angular deps: []
		 */
		function fixNameCase(name) {
			var ret = name
				.split(' ')
				.map(function(word) {
					return word[0].toLocaleUpperCase() + word.substring(1);
				})
				.join(" ");

			if (ret != name) {
				console.log("Applied case fix: '"+name+"' -> '"+ret+"'");
			}
			
			return ret;
		}


		/* basename:
		 * Gets basename from path
		 * angular deps: []
		 */
		function basename(path) {
			return path
				.replace(/\\/g, '/')
				.replace(/.*\//, '');
		}


		/* parseWikipediaContent:
		 * Parse buffer using data from wpData
		 * angular deps: []
		 */
		function parseWikipediaContent(buffer, wpData) {
			var pages = buffer.query.pages;
			
			/* Check for no-page */
			if (-1 in pages) {
				throw 'NO_PAGE';
			}

			var id = undefined;
			for (var id_ in pages) {
				id = id_;
				break;
			}

			var revNumber = pages[id].revisions.length - 1;
			var content = pages[id].revisions[revNumber]['*'];

			/* Check for redirection */
			if (content.match(/^#REDIRECT \[\[(.+?)\]\]/)) {
				throw 'WP_REDIRECT';
			}

			var birth = '';
			var death = '';
			content.split('\n').forEach(function(line) {
				var birthMatch = line.match(wpData.birthRegexp);
				var deathMatch = line.match(wpData.deathRegexp);

				if (birthMatch && !birth) {
					birth = birthMatch[1];
				}
				if (deathMatch && !death) {
					death = deathMatch[1];
				}
			});

			/* This is a person? */
			if (!birth && !death) {
				throw 'NOT_A_PERSON';
			}

			return {
				'name': pages[id].title,
				'alive': death ? false : true,
				'birthDate': birth || undefined,
				'deathDate': death || undefined,
				'url': '//' + wpData.urlPrefix + '.wikipedia.org/wiki/' + pages[id].title
			};
		}


		/* _:
		 * Wrapper around gettext
		 * angular deps: [$translate]
		 */
		function _ (msgId) {
			return gettext($translate, msgId, arguments[1] || {})
		}


		/* getLanguage:
		 * Returns current user language
		 * angular deps: [$translate]
		 */
		function getLanguage() {
			var languageParam = $location.search().lang;
			if (languageParam === undefined) {
				languageParam = $translate.use();
			}
			return languageParam;
		}


		/* getWikipedia:
		 * Retrieves (as promise object) the wikipedia content for 'name' in 'language'
		 * angular deps: [$q, $http]
		 */
		function getWikipedia(name, language) {
			var deferred = $q.defer();

			var apiUrl = '//' + language + '.wikipedia.org/w/api.php?';
			apiUrl += 'action=query&prop=revisions&rvprop=content&format=json&callback=JSON_CALLBACK&section=0&redirects&';
			apiUrl += 'titles=' + name;

			$http.jsonp(apiUrl)
				.success(deferred.resolve)
				.error(deferred.reject);
			
			return deferred.promise;
		}


		/* getSuggestions:
		 * Retrieves (as promise object) the suggestions for 'name' in 'language'
		 * angular deps: [$q, $http]
		 */
		function getSuggestions(name, language) {

			function onApiCallSuccess (data) {
				var suggestions = data.responseData.results
					.map(function (suggestion) {
						return decodeURIComponent(basename(suggestion.unescapedUrl).replace(/_/g, ' '));
					})
					.filter(function(e) {
						return e != name;
					});
				
				if (suggestions.length == 0) {
					deferred.reject("No suggestions");
				}
				else {
					deferred.resolve(suggestions);
				}				
			}

			var deferred = $q.defer();

			var apiUrl = '//ajax.googleapis.com/ajax/services/search/web?';
			apiUrl += 'v=1.0&callback=JSON_CALLBACK&q=site:'+language+'.wikipedia.org%20'+name;

			$http.jsonp(apiUrl)
				.success(onApiCallSuccess)
				.error(console.log);

			return deferred.promise;
		}


		/* doSearch:
		 * Perform search
		 * angular deps: [$scope]
		 */
		function doSearch(queryParam, languageParam, opts) {
			function onSearchDone(result) {
				var wpParsed = undefined;
				try {
					wpParsed = parseWikipediaContent(result, WP_DATA[languageParam]);
				}
				catch (e) {
					console.log("[search] wikipedia:"+languageParam+" for "+queryParam+" parse failed: "+e);
					tryNextSearch();
					return;
				}

				if ('redirect' in wpParsed) {
					$location.path("/s/"+wpParsed.redirect);
					return;
				}
				$scope.busy = false;
				$scope.result = wpParsed;
				$scope.suggestions = [];
				$scope.error = '';
				$scope.query = '';
				console.log(wpParsed);
			}

			function onSuggestionsDone(suggestions) {
				console.log("[suggestions] wikipedia:"+languageParam+" for "+queryParam+" has "+suggestions.length+" suggestions");
				$scope.busy = false;
				$scope.suggestions = suggestions;
			}

			function onSuggestionsFail(e) {
				console.log("suggestions failed: "+e);
			}

			function tryNextSearch() {
				// Jump to next search, the english wikipedia
				if (languageParam != 'en') {
					doSearch(queryParam, 'en', opts);
					return;
				}

				// All wikipedia searches have failed at this point, let's show the user some
				// suggestions
				if (opts.suggestions === true) {
					getSuggestions(queryParam, getLanguage())
						.then(onSuggestionsDone)
						.catch(onSuggestionsFail);
					return;
				}

				// With all searches failed and no suggestions allowed user has no options here
				$scope.busy = false;
				$scope.query = '';
				$scope.error = _("NO_INFORMATION_ERROR", {'who': queryParam})
			}

			console.log("[search] wikipedia:"+languageParam+" for "+queryParam+" started");
			window.ga('send', 'pageview', {
				'page': $location.path()
			});
			$scope.busy = true;
		
			getWikipedia(queryParam, languageParam)
				.then(onSearchDone)
				.catch(tryNextSearch);
		}


		/* $scope.isDeadOrAliveFromQuery:
		 * Triggers the seach for model query
		 * angular deps: [$scope, $location]
		 */
		$scope.isDeadOrAliveFromQuery = function() {
			var fixedQuery = fixNameCase($scope.query);
			$location.path('/s/' + fixedQuery).search('');
		}


		/* queryParam: holds the 'name' or the 'person' */
		var queryParam = $routeParams.query;
		if (queryParam !== undefined) {
			$scope.query = queryParam;
		}

		/* languageParam: Holds the user choosen language */ 
		var languageParam = getLanguage();

		/* suggestionsParam: Show (or) not suggestions if there isn't a result */
		var suggestionsParam = !('ns' in $location.search());

		if (queryParam) {
			doSearch(queryParam, languageParam, {'suggestions': suggestionsParam});
		}

	}
]);
